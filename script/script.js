//Теоретичні питання

//1.Описати своїми словами навіщо потрібні функції у програмуванні.
//Функції призначені для спрощення коду, вони мають ряд переваг, які роблять їх надзвичайно корисними.

//2.Описати своїми словами, навіщо у функцію передавати аргумент.

//3.Що таке оператор return та як він працює всередині функції?
//Оператор return завершує виконання поточної функції та повертає її значення.

// Завдання 

function getNumber() {
    let inputNumber = +prompt("Введіть число").trim()
// Перевірка введеного числа
    while (isNaN(inputNumber)) { 
        alert("Дані введено не коректно, спробуйте ще раз")
        inputNumber = +prompt("Введіть число", [inputNumber]).trim()
    }
    return inputNumber
}

const getMathOperationSign = () => {
    let result = prompt("Оберіть операцію яку потрібно виконати (+, -, *, /)").trim()
    while (!isValid(result)) {
        result = prompt("Дані введено не коректно, виберіть одну з операцій (+, -, *, /)", [result]).trim()
    }
// Перевірка математичного знаку через includes
    function isValid(operation) { 
        return "+ - * /".includes(operation);
    }
    return result
}

function getCalc(a, b, mathSign) {
    switch (mathSign) {
        case "+":
            return a + b
        case "-":
            return a - b
        case "*":
            return a * b
        case "/":
            return a / b
    }
}

const doMath = getCalc(getNumber(), getNumber(), getMathOperationSign())
console.log(doMath)
